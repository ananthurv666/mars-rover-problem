require_relative "./rover"
require_relative "./operator"

plateau = ARGF.gets

while !ARGF.eof
  rover_position = ARGF.gets
  position = rover_position.split[0, 2]
  direction = Directions.new(rover_position.split[2])
  rover = Rover.new(direction, position)

  commands = ARGF.gets
  Operator.new(rover).receive commands

  puts rover
end
